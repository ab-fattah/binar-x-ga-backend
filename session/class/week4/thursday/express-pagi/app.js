const express = require('express');
const app = express();
const PORT = 3000;

const router = require('./routes')

//Middlewares
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended:false}));

app.use(router); //Router

app.listen(PORT, () => {
    console.log("Server running at port : ", PORT);
});