const { Router } = require('express');
const router = Router();
const StudentRoutes = require('./student');

router.get('/', (req, res) => {
    // res.send("Ini page home");
     res.render('index.ejs');
});

router.use('/students', StudentRoutes);

// router.get('/lectures', (req, res) => {
//     res.send("Ini page lectures")
// })

module.exports = router;