'use strict';
const { encryptPwd } = require('../helpers/bcrypt')

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user.hasMany(models.product)
    }
  };
  user.init({
    username: {
      type : DataTypes.STRING,
    validate : {
      notEmpty : {
        msg : "Username must be filled! thanks."
      },
      isEmail : {
        msg : "Username must be email format."
      }
    }
  },
    password: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Password must be filled! thanks."
        }
      }
    }
  }, {
    hooks : {
      beforeCreate(users) {
        users.password = encryptPwd(users.password)
      }
    },
    sequelize,
    modelName: 'user',
  });
  return user;
};