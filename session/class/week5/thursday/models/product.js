'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      product.belongsTo(models.user)
    }
  };
  product.init({
    name: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Product name must be filled! thanks."
        }
      }
    },
    info: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Product info must be filled! thanks."
        }
      }
    },
    image: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Product image must be filled! thanks."
        },
        isUrl : {
          msg : "Product image must be url format."
        }
      }
    },
    price: {
      type : DataTypes.INTEGER,
      validate : {
        notEmpty : {
          msg : "Product price must be filled! thanks."
        },
        isNumeric : {
          msg : "Price must be a number."
        }
      }
    },
    stock: {
      type : DataTypes.INTEGER,
      validate : {
        notEmpty : {
          msg : "Product stock must be filled! thanks."
        },
        isNumeric : {
          msg : "Stock must be a number."
        }
      }
    },
    userId: DataTypes.INTEGER
  }, {
    hooks : {
      beforeCreate(product, options) {
        product.stock = 1;
      }
    },
    sequelize,
    modelName: 'product',
  });
  return product;
};