const { user } = require('../models');
const { decryptPwd } = require('../helpers/bcrypt');
const { tokenGenerator } = require('../helpers/jwt');

class UserController {
    static async menu (req, res) {
        try {
            const users = await user.findAll()

                res.status(200).json(users);
        } catch (err) {
            res.status(500).json(err);
        }
    }
    
    static async login (req, res) {
        const { username, password } = req.body;

        try {
            const userFound = await user.findOne({
                where : {
                    username
                }
            })
            if(userFound) {
                if (decryptPwd(password, userFound.password)) {
                    const access_token = tokenGenerator(userFound)
                    res.status(200).json({access_token})
                } else {
                    throw {
                        status : 400,
                        msg : "Pwd is not the same"
                    }
                }
            } else {
                throw {
                    status : 404,
                    msg : "user is not Found"
                }
            }
        }catch (err) {
            res.status(500).json(err);
        }
    }

    static async register (req, res) {
        const { username, password } = req.body;

        try {
            const users = await user.create({
                username, password
            })
            res.status(201).json(users)
        } catch (err) {
            res.status(500).json(err);
        }
    }
}


module.exports = UserController