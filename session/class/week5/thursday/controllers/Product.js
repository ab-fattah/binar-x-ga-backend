const { product, user } = require('../models');

class ProductController {
    static async getProduct (req, res, next) {
        try {
            const result = await product.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include: [
                    user
                ]
            })
            res.status(200).json(result);
        } catch (err) {
            // res.status(500).json(err);
            next(err)
        }
    }
    static async addProduct (req, res, next) {
        const { name, info, image, price, stock } = req.body;
        const userId = req.userData.id;
        try {
            const found = await product.findOne({
                where: {
                    name
                }
            })
            if (found) {
                res.status(409).json({
                    msg: "Name already exist! Try another name, thanks"
                })
            } else {
                const products = await product.create({
                    name, info, image, price, stock, userId
                })

                res.status(201).json(products)
            }
        }catch (err) {
            // res.status(500).json(err)
            next(err)
        }
    }

    static async deleteProduct (req, res, next) {
        const id = req.params.id;

        try {
            const result = product.destroy({
                where: {
                    id
                }
            })
            res.status(200).json({
                result,
                msg: "Product deleted"
            })
        }catch (err) {
            // res.status(500).json(err)
            next(err)
        }
    }

    static async updateProduct (req, res) {
        
    }
}



module.exports = ProductController