const { product } = require('../models');
const { tokenVerifier } = require('../helpers/jwt');

const authentication = (req, res, next) => {
    console.log('Authentication works!');

    const { access_token } =req.headers;
    if(!access_token) {
        res.status(404).json({
            msg : "Token not found!"
        })
    } else {
        try {
            const decode = tokenVerifier(access_token)
            req.userData = decode
            next();
        } catch (err) {
            res.status(400).json(err)
        } 
    } 
}

const authorization = (req, res, next) => {
    console.log('Authoriziation works!');
    const id = req.params.id;
    const userId = req.userData.id;

    product.findOne({
        where : {
            id
        }
    }).then (products => {
        if (product) {
            if (products.userId === userId) {
                next();
            } else {
                throw {
                    status : 403,
                    msg : "User doesn't have any access"
                }
            }
        } else {
            throw {
                status : 404,
                msg : "Product not Found"
            }
        }
    }).catch(err => {
        res.status(500).json(err)
    })
}

module.exports = {
    authentication, authorization
}