'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class player extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  player.init({
    name:  {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Name must be filled! thanks."
        }
      }
    },
    info: {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Info must be filled! thanks."
        }
      }
    },
    image: {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Image must be filled! thanks."
        },
        isUrl : {
          msg : "Image must be URL format!"
        }
      }
    },
    level: {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Level must be filled! thanks."
        }
      }
    },
  }, {
    hooks : {
      beforeCreate(player, options) {
        player.level = 1;
      }
    },
    sequelize,
    modelName: 'player',
  });
  return player;
};