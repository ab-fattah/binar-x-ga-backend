'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class weapon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  weapon.init({
    name: {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Name must be filled! thanks."
        }
      }
    },
    info: {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Info must be filled! thanks."
        }
      }
    },
    image: {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Image must be filled! thanks."
        },
        isUrl : {
          msg : "Image must be URL format!"
        }
      }
    },
    power: {
      tipe:DataTypes.STRING,
      validate : {
        notEmpty: {
          msg : "Name must be filled! thanks."
        }
      }
    },
  }, {
    sequelize,
    modelName: 'weapon',
  });
  return weapon;
};