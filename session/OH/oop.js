// DRY - Do not Repeat Yourself
// WET - Write Everything Twice

// OOP
class Person {
  constructor(name, age, gender) {
    this.name = name; // public property
    this.age = String(age);
    this.gender = gender;
    this.greet = function () {
      // public method
      console.log(`Halo ${this.name}!`);
    };
    this.ID = this.name + this.age; // public property
    var countryCode = "IDR"; // private property
  }

  // static number = 1; // static property
  static getCountryCode() {
    // static method
    console.log("Country code is...");
  }
}

let Person1 = new Person("Apson", 17, "male");
// console.log(Person1, "--person 1");
// Person.getCountryCode();
// console.log(Person.number);

// if we don't use a "this"
function createNewPerson(name) {
  const obj = {};
  obj.name = name;
  obj.greet = function () {
    return `Good Morning, I am ${obj.name}`;
  };

  return obj;
}

// use this
function createNewPersonThis(name) {
  this.name = name;
  this.greet = function () {
    return `Good Morning, I am ${obj.name}`;
  };
}

let Person2 = createNewPerson("Daniel");
console.log(Person2, "--person 2");

let Person3 = new createNewPersonThis("Fanny");
console.log(Person3 instanceof createNewPersonThis, "--person 3");
