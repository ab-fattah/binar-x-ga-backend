const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      trim: true,
      uppercase: true,
      minlength: 5,
      maxlength: 20,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      default: "User",
    },
    isActive: {
      type: Boolean,
      default: 1,
    },
    isMerchant: {
      type: String,
      default: "Buyer",
    },
  },
  { timestamps: true, versionKey: false }
);

module.exports = mongoose.model("Users", UserSchema, "Users");
