class Password {
    constructor(word) {
        this.input = word
        this.data = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    }

    _generate () {
        let temp = ''
        for (let i=0; i< this.input.length; i++) {
            const index = Math.floor(Math.random() * 25)
            temp += this.data[index]
        }
        return temp
    }


    generate() {
        // console.log('generating new password')
        return this._generate()
    }
}

const myPass = new Password('hhhhh')
// myPass._setValue('abcqweqw')
console.log(myPass.generate())
console.log(myPass._generate())




// abstraction a way / a method to hide a complex process.
/*
Abstraction has to do with separating interface from implementation.
(We don't care what it is, we care that it works a certain way.)

Encapsulation has to do with disallowing access to or knowledge of internal structures of an implementation.
(We don't care or need to see how it works, only that it does.)

https://stackoverflow.com/questions/25029465/whats-the-difference-between-abstraction-and-encapsulation

*/