const express = require("express")
const router = express.Router()

const productControllers = require("../controllers/product")
const {Authentication} = require("../middlewares/auth")


router.get("/", Authentication,productControllers.GetAllProducts)
router.post("/register",Authentication ,productControllers.Register)
router.put("/edit/:id",Authentication, productControllers.Edit)


module.exports = router