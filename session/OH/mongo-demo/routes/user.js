const express = require("express")
const router = express.Router()

const userControllers = require("../controllers/user")
const {Authentication} = require("../middlewares/auth")


router.get("/", Authentication,userControllers.GetAllUsers)
router.post("/register", userControllers.Register)
router.post("/login", userControllers.Login)
router.put("/edit/:id",Authentication, userControllers.Edit)


module.exports = router