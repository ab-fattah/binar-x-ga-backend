const {Product} = require("../models/product")


exports.Register = async (req, res, next) => {
    try{
        console.log(req.userData, "--user data from token")

        req.body.users = req.userData._id

        let data = await Product.create(req.body);

        res.status(201).json({
            success: true,
            message: "Successfully create a product!",
            data
        })

    } catch(err) {
        next(err)
    }
}


exports.GetAllProducts = async(req, res, next) => {
    try{
        let data = await Product.find().populate("users")

        res.status(200).json({
            success: true,
            message: "Successfully get all products!",
           data
        })

    } catch(err) {
        next(err)
    }
}

exports.Edit = async(req, res, next) => {
    try {
        const {id} = req.params

        if (!id) return next({message: "Missing ID Params"})

        const product = await Product.findById(id)

        if (!product) return next({message: `There is no product with _id:${id}`})

        const updatedUser = await Product.findByIdAndUpdate(id, 
          {$set: req.body},
          {new: true}
        )

        res.status(200).json({
            success: true,
            message: "Successfully update a product!",
            data: updatedUser,
          });

    } catch(err) {
        next(err)
    }
}