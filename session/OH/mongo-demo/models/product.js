const mongoose = require("mongoose");
const {Schema} = mongoose;


const productSchema = new Schema({
  name: {
      type: String,
      trim: true,
      minlength: 4
  },
  user: [{type: Schema.Types.ObjectId, ref: "User"}]    
}, {
    timestamps: true,
    versionKey: false
})


const product = mongoose.model("Product", productSchema)

exports.Product = product