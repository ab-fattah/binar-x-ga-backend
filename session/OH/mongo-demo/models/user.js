const mongoose = require("mongoose");
const {Schema} = mongoose;

// encrypt pwd
const {encryptPassword} = require("../helpers/bcrypt")

const userSchema = new Schema({
  email: {
      type: String,
      lowercase: true,
      trim: true,
      required: true
  },
  password: {
      type: String,
      required: true
  },
  fullName: {
      type: String,
      trim: true,
      minlength: 4
  },
  products : [{type: Schema.Types.ObjectId, ref: "Product"}]  
}, {
    timestamps: true,
    versionKey: false
})

userSchema.pre("save", async function(next) {
    let user = this
    if (user.password && user.isModified("password")) {
        user.password = await encryptPassword(user.password)
    }
    next()
})

const user = mongoose.model("User", userSchema)

exports.User = user