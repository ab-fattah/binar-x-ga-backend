// Pake inheritance kalo mau buat subclass yang mau nambahin karakteristik/property ga dipunyain parentnya
class Animal {
  constructor(type, isMammal) {
    this.type = type; // sea, land, air
    this.isMammal = isMammal; // true || false
  }

  static speak() {
    // parent method
    console.log("Hi I am Animal!");
  }
}

class Fish extends Animal {
  constructor(name) {
    super("sea", true);
    this.name = name;
  }

  shout() {
    console.log("Hi I am Fish!");
  }
}

let koi = new Fish("Koi");
console.log(koi, "--koi");
koi.shout();
