const express = require('express')
const router = express.Router()
const { home, login } = require('../controller/index') // need to test


const validateData = (req, res, next) => {
    console.log('user is valid')
    return next()
}

// get, post, put, delete
router.get('/home', home)
router.get('/login', validateData, login)

// utk Rest API, nama endpoint ga boleh mengandung verb.
// REST API Rules

module.exports = router