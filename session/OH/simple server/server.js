const express = require('express')
const server = express()

const { handleError } = require('./errHandler')

// routes
const route = require('./routes/index') // need to test
const user = require('./routes/user')


// server.get('/', (req, res) => {
//     res.status(200).send('this is our home page')
// })

server.use(route)
server.use(user)
server.use(handleError)

const PORT = 3000
server.listen(PORT, () => console.log(`server run on port ${PORT}`))

module.exports = server