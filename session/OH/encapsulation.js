// untuk membungkus properti / method yang sensitif kepada user
class Calculator {
  constructor(num1, num2, berat) {
    this._num1 = num1;
    this.num2 = num2;
    var harga = 10000;
    this.berat = berat;
    this.std = function () {
      return (1 / num1) ^ (2 * num2);
    };
    this.ongkir = function () {
      return harga * berat;
    };
  }

  // sum
  sum() {
    console.log(this.num1 + this.num2);
  }
  // divide
  divide() {
    console.log(this.num1 / this.num2);
  }
}

let calc1 = new Calculator(1, 2);
// console.log(calc1.std(), "--calc 1");

calc1.sum();
calc1.num2();

let ongkir = new Calculator(1, 2, 5);
ongkir.ongkir = function () {
  return 1000;
};
console.log(ongkir);
console.log(ongkir.ongkir());

var str = "1";
var num = 1;

var arr = [];
