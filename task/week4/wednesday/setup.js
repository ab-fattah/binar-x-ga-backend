const pool = require('./config/connection');

const toDoTask = `
CREATE TABLE todo (
    id SERIAL PRIMARY KEY,
    task VARCHAR(50),
    status VARCHAR(50),
    tag VARCHAR(50),
    created_at DATE,
    completed_at DATE
);
`;

pool.query(toDoTask, (err, data ) => {
    if (err) {
        throw err;
    } else {
        console.log('Table "todo" created succesfully');
        pool.end();
    }
})