const ToDoController = require('./controller/toDoController');
const instruction = process.argv[2];
const parameter = process.argv.slice(3);

switch(instruction) {
    case 'help':
        ToDoController.help()
        break;
    case 'list':
        ToDoController.list();
        break;
    case 'add':
        ToDoController.add(parameter);
        break; 
    case 'delete':
        ToDoController.delete(parameter);
        break;
    case 'update':
        ToDoController.update(parameter);
        break;
    case 'complete':
        ToDoController.complete(parameter);
        break;
    case 'uncomplete':
        ToDoController.uncomplete(parameter);
        break;
    default:
        ToDoController.message();
        break;
}