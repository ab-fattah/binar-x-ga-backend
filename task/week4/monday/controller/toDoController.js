const ToDoModel = require('../model/toDoModel');
const ToDoView = require('../view/toDoView');

class ToDoController {

    static help() {
        const help = ToDoModel.help();
        ToDoView.help(help);
    }
    static list() {
        const list = ToDoModel.list();
        ToDoView.list(list);
    }

    static add(parameter) {
        const result = ToDoModel.add(parameter);
        ToDoView.message(result);
    }

    static delete(parameter) {
        const result = ToDoModel.delete(parameter);
        ToDoView.message(result);
    }

    static update(parameter) {
        const result = ToDoModel.update(parameter);
        ToDoView.message(result);
    }

    static complete(parameter) {
        const result = ToDoModel.complete(parameter);
        ToDoView.message(result);
    }

    static uncomplete(parameter) {
        const result = ToDoModel.uncomplete(parameter);
        ToDoView.message(result);
    }

    static message() {
        ToDoView.message('Masukkan Command yang benar!')
    }
}






module.exports = ToDoController;