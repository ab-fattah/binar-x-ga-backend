const fs =require('fs');

class ToDoModel {
    constructor(id, task, status, tag, created_at, completed_at) {
        this.id = id;
        this.task = task;
        this.status = status;
        this.tag = tag || [];
        this.created_at = created_at;
        this.completed_at = completed_at;
       
    }

    
    // tag() {
    //     if(this.status === true) {
    //     return this.tag.push([X]);
    // } else {
    //     return this.tag.push([O]);
    // }

    // }

    static help() {
         const help = [
            'node todo.js',
            'node todo.js help',
            'node todo.js list',  
            'node todo.js add <task>',
            'node todo.js update <id> <task>',
            'node todo.js delete <id>',
            'node todo.js complete <id>',
            'node todo.js uncomplete <id>'
        ]

        return help;
    }

    static list() {
        const doc = fs.readFileSync('./data.json', 'utf-8');
        const parseDoc = JSON.parse(doc);

        let toDoList = [];
        parseDoc.forEach(doc => {
            toDoList.push(new ToDoModel(doc.id, doc.task, doc.status, doc.tag, doc.created_at, doc.completed_at));
        });
        return toDoList;
    }

    static add(parameter) {
        const todos = this.list();
        const [task, status, tag] = parameter;

        const newId = todos[todos.length -1].id + 1;
        const newToDo = {
            id : newId,
            task : task,
            status : (status === 'true'),
            tag : tag || [],
            created_at : new Date(),
            completed_at : new Date()

            
        }
        // switch(status) {
        //     case 'true':
        //         this.completed_at = new Date();
        //         break;
        //     case 'false':
        //         this.completed_at = null;
        //         break;
        // }
        todos.push(newToDo);
        
        this.save(todos)
        return `${task} has been create!!`
    }

    static delete(parameter) {
        const todos = this.list();
        const id = Number(parameter[0]);

        const newToDo = todos.filter(el => el.id !== id);

        this.save(newToDo);
        return `${id} has been remove!`
    }

    static update(parameter) {
        const todos = this.list();
        // return todos;
        const id = Number(parameter[0]);
        const name = parameter[1];

        todos.forEach(el => {
            if(el.id === id) {
                el.task = name;   
            }
        });

        this.save(todos);
        return `${id} has been change to ${name}`;
    }

    static complete(parameter) {
        const todos = this.list();
        let task = "";

        todos.forEach(el => {
        if(el.id === Number(parameter)) {
            task = el.task;
            el.status = true;
            el.tag = 'x';
            el.completed_at = new Date();
        }
        })
        this.save(todos);
        return `${task} has been completed!`
    }

    static uncomplete(parameter) {
        const todos = this.list();
        let task = "";

        todos.forEach(el => {
            if(el.id === Number(parameter)) {
                task = el.task;
                el.status = false;
                el.tag = []
                el.completed_at = null;
            }
            })
            this.save(todos);
            return `${task} has not been completed!`


    }

    static save(doc) {
        fs.writeFileSync('./data.json', JSON.stringify(doc, null, 2));
    }

}





module.exports = ToDoModel;