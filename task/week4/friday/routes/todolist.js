const { Router } = require('express');
const router = Router();
const TodoController = require('../controllers/Todolist');

router.get('/', TodoController.getTodo);
router.get('/add', TodoController.addFormTodo);
router.post('/add', TodoController.addTodo);
router.get('/delete/:id', TodoController.deleteTodo);
router.get('/edit/:id', TodoController.updateFormTodo);
router.post('/edit/:id', TodoController.updateTodo);
router.get('/:id', TodoController.findById);


module.exports = router;