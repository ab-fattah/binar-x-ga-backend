const { Router } = require('express');
const router = Router();
const TodoRoutes = require('./todolist');


router.get('/', (req, res) => {
    // res.send("Ini page home");
     res.render('index.ejs');
});

router.use('/todolists', TodoRoutes);


module.exports = router;