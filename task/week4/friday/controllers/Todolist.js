const { Todolists } = require('../models');

class TodoController {
    static getTodo (req, res) {
        Todolists.findAll({
            order : [
                ['id', 'ASC']
            ]
        })
            .then(result => {
                res.render('todolists.ejs', { todolists: result })
            })
            .catch(err => {
                console.log(err);
            })
    }

    static addFormTodo (req, res) {
        res.render('addTodo.ejs');
    }

    static addTodo (req, res) {
        const { task, status } = req.body;

        Todolists.findOne ({
            where : {
                task
            }
        })
            .then(found => {
                if(found) {
                    res.send('Name already exist!')
                } else {
                    Todolists.create({
                        task,
                        status
                    })
                }
            })
        
            .then(result => {
                res.redirect('/todolists')
            })
            .catch(err => {
                res.send(err);
            })
    }

    static findById (req, res) {
        const id = req.params.id;
        Todolists.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteTodo (req, res) {
        const id = req.params.id;
        Todolists.destroy({
            where: { id }
        })
            .then(() => {
                res.redirect('/todolists')
            })
            .catch(err => {
                res.send(err);
            })
    }

    static updateFormTodo(req, res) {
        const id = req.params.id;
        Todolists.findOne({
            where: { id }
        })
        .then(result => {
            res.render('editTodo.ejs', { todolists: result });
        })
        .catch (err => {
            res.send(err)
        })
    }

    static updateTodo(req, res) {
        const id = req.params.id;
        const { task, status } = req.body;

        Todolists.update({
            task,
            status
        }, {
            where: { id }
        })
            .then(result => {
                if (result[0] === 1) {
                    res.redirect('/todolists')
                } else {
                    res.send('Update failed!')
                }
            })
            .catch(err => {
                res.send(err)
            })
    }

}


module.exports = TodoController;