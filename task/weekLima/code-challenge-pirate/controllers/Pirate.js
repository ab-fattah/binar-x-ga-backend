const { Pirates } = require('../models');

class PirateController {
    static getPirate (req, res) {
        Pirates.findAll({
            order : [
                ['id', 'ASC']
            ]
        })
            .then(result => {
                res.render('pirates.ejs', { pirates: result })
            })
            .catch(err => {
                console.log(err);
            })
    }

    static addFormPirate(req, res) {
        res.render('addPirate.ejs');
    }

    static addPirate (req, res) {
        const { name, status, haki } = req.body;

        Pirates.findOne ({
            where : {
                name
            }
        })
        
            .then(found => {
                if (found) {
                    res.send("Name already exist! Try another name")
                } else {
                    Pirates.create({
                        name,
                        status,
                        haki
                    })
                }
               
            })
            .then (result => {
                 // res.send('add succesfull')
                 res.redirect('/pirates')
            })
            .catch(err => {
                res.send(err);
            })
    }

    static findById (req, res) {
        const id = req.params.id;
        Pirates.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deletePirate(req, res) {
        const id = req.params.id;
        Pirates.destroy({
            where: { id }
        })
            .then(() => {
                res.redirect('/pirates')
            })
            .catch(err => {
                res.send(err);
            })
    }

    static editFormPirate (req, res) {
        const id = req.params.id;
        Pirates.findOne({
            where : { id }
        })
        .then (result => {
            res.render('editPirate.ejs', { pirates: result})
        })
        .catch(err => {
            res.send(err);
        })
    }

    static editPirate (req, res) {
        const id = req.params.id;
        const { name, status, haki } = req.body;

        Pirates.update ({
            name,
            status,
            haki
        }, {
            where: { id }
        })
        .then(result => {
            if (result[0] === 1) {
                res.redirect('/pirates')
            } else {
                res.send('Update failed!')
            }
        })
        .catch(err => {
            res.send(err)
        })
    }

}


module.exports = PirateController;