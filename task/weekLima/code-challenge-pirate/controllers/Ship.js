const { Ships } = require('../models');

class ShipController {
    static getShip (req, res) {
    Ships.findAll({
        order : [
            ['id', 'ASC']
        ]
    })
            .then(result => {
                res.render('ships.ejs', { ships: result })
            })
            .catch(err => {
                console.log(err);
            })
    }

    static addFormShip (req, res) {
        res.render('addShip.ejs');
    }

    static addShip (req, res) {
        const { name, type, power } = req.body;

        Ships.findOne ({
            where : {
                name
            }
        })
        
            .then(found => {
                if (found) {
                    res.send("Name already exist! Try another name")
                } else {
                    Ships.create({
                        name,
                        type,
                        power
                    })
                }
               
            })
            .then (result => {
                 // res.send('add succesfull')
                 res.redirect('/ships')
            })
            .catch(err => {
                res.send(err);
            })
    }

    static findById (req, res) {
        const id = req.params.id;
        Ships.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteShip (req, res) {
        const id = req.params.id;
        Ships.destroy({
            where: { id }
        })
            .then(() => {
                // res.send('delete succesfull')
                res.redirect('/ships')
            })
            .catch(err => {
                res.send(err);
            })
    }

    static editFormShip (req, res) {
        const id = req.params.id;
        Ships.findOne({
            where : { id }
        })
        .then (result => {
            res.render('editShip.ejs', { ships: result})
        })
        .catch(err => {
            res.send(err);
        })
    }

    static editShip (req, res) {
        const id = req.params.id;
        const { name, type, power } = req.body;

        Ships.update ({
            name,
            type,
            power
        }, {
            where: { id }
        })
        .then(result => {
            if (result[0] === 1) {
                res.redirect('/ships')
            } else {
                res.send('Update failed!')
            }
        })
        .catch(err => {
            res.send(err)
        })
    }

}

let i = 10;
module.exports = ShipController;