'use strict';
const fs = require('fs')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const parseData = JSON.parse(fs.readFileSync('./pirates.json'));
   const pirateData = [];
   parseData.forEach(data => {
     const { name, status, haki } = data;
     pirateData.push({
       name,
       status,
       haki,
       createdAt : new Date(),
       updatedAt : new Date()
     })
   })
  await queryInterface.bulkInsert('Pirates', pirateData, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Pirates', null, {});
  }
};
