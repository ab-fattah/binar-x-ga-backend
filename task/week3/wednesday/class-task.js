class Student {
    constructor(name, age, dateOfBirth, gender, hobbies) {
        this._name = name;
        this._age = age;
        this._dateOfBirth = dateOfBirth;
        this._id = this._name + this._age;
        this._gender = gender;
        this._hobbies = hobbies || []
    }

    get name () {
        return this._name;
    }

    set setName (name) {
        this._name = name;
    }

    set setAge (age) {
        this._age = age;
    }

    set setDateOfBirth (date) {
        this._dateOfBirth = date;
    }

    set setGender (gender) {
        if (gender === 'male' || gender === 'female') {
            this._gender = gender;
        } else {
            this._gender = " ";
        }
    }

    addHobby (hobby) {
        this._hobbies.push(hobby);
    }

    removeHobby (rhobby) {
        for(let i = 0; i < this._hobbies.length; i++) {
            if (this._hobbies[i] === rhobby) {
                this._hobbies.splice(i, 1)
            }
        }
    }

    getData () {
        return this;
    }

}

const james = new Student ('james', 16, '21 Januari 2004', 'male', ['Basketball', 'Swimming']);

const karina = new Student ('karina', 20, '2 mei 2000', 'female', ['Travelling', 'shopping']);
// // james._name = 'andi';
// james.gender ='male';
karina.addHobby('coding')
// console.log(james);
karina.removeHobby('shopping')
console.log(karina);
console.log(james.getData());
