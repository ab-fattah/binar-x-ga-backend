//factory class for Army
class Barrack {
    constructor(slots) {
        this._slots = slots || []
    }

//getter
    get slots() {
        return this._slots;
    }

//setter
    set setSlots(slot) {
        this._slots = slot;
    }

//method
// add instance of Army
    recruit(army){
      this._slots.push(army);  
      console.log(`${army.name} Join!`)
    }

    summon(){
//grouping by batalyon
        let grouping = {
            knight: [],
            spearman: [],
            archer: []
        }
        
        this._slots.forEach(el => {
            if(el._batalyon === 'knight') {
                grouping.knight.push(el)
            } else if(el._batalyon === 'spearman') {
                grouping.spearman.push(el)
            } else {
                grouping.archer.push(el)
            }
        })

        console.log("========== Ready to Win ==========")
        console.log(grouping);
    }

//delete instance of Army
    disband(name){
        for(let i = 0; i < this._slots.length; i++) {
            if (this._slots[i].name === name) {
                this._slots.splice(i, 1);
                console.log(`${name} disband!`)
            }
        }  
    }

}

module.exports = Barrack;