//import from army.js and barrack.js
const {Knight, Spearman, Archer} = require('./army.js')
const Barrack = require('./barrack.js');

//create Knight Batalyon
let knight1 = new Knight('James', 28, 'knight', 4, 1);
knight1.training();
knight1.talk();

let knight2 = new Knight('Sirius', 26, 'knight', 9, 1);
knight2.training();
knight2.talk();

//create Spearman Batalyon
let spearman1 = new Spearman('Harry', 21, 'spearman', 1, 1);
spearman1.training();
spearman1.talk();

let spearman2 = new Spearman('Ron', 23, 'spearman', 7, 1);
spearman2.training();
spearman2.talk();

//create Archer Batalyon
let archer1 = new Archer('Dumbledore', 34, 'archer', 2, 1)
archer1.training();
archer1.talk();
let archer2 = new Archer('Severus', 31, 'archer', 11, 1)
archer2.training();
archer2.talk();

//create Barrack for Agrippa's Army
const newBarrack = new Barrack();

//recruit Army
newBarrack.recruit(knight1);
newBarrack.recruit(knight2);
newBarrack.recruit(spearman1);
newBarrack.recruit(spearman2);
newBarrack.recruit(archer1);
newBarrack.recruit(archer2);

//grouping Army by Batalyon and save it to slots
newBarrack.summon();

//delete Army from Barrack
newBarrack.disband('Ron');

//check group
newBarrack.summon();

//Agrippa's Army Ready to Win!!