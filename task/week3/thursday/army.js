//Parent Class
class Army {
    constructor(name, age, batalyon, division, level) {
        this._name = name;
        this._age = age;
        this._batalyon = batalyon;
        this._division = division;
        this._level = level || 1;
    }

//getter -> get value from property
    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get batalyon() {
        return this._batalyon;
    }

    get division() {
        return this._division;
    }

    get level() {
        return this._level;
    }

//setter -> set property value
    set setName(name) {
        this._name = name;
    }

    set setAge(age) {
        this._age = age;
    }

    set setBatalyon(batalyon) {
        this._batalyon = batalyon;
    }

    set setDivision(division) {
        this._division = division;
    }

    set setLevel(level) {
        this._level = level;
    }

//method
    talk() {
        console.log(`i am ${this._name} from batalyon ${this._batalyon} , i am on level ${this._level}.`)
    };

    training() {
        let maxLevel = 10
        this._level++
        if(this._level < maxLevel) {
            console.log(`${this._name} level up`)
        } else {
            console.log(`${this._name} on high level`)
        }
    }
};

//sub class / instance of Army class
class Knight extends Army {
    constructor(name, age, batalyon, division, level) {
        super(name, age, batalyon, division, level)
    }

    talk() {
//overriding
        super.talk()
        if(this._division <= 5) {
            console.log(`I am from division ${this._division} "${this._batalyon}" main force, ready to win`)
        } else {
            console.log(`I am from division ${this._division} "${this._batalyon}" support troops, ready to help main force`)
        }
    }
}

class Spearman extends Army {
    constructor(name, age, batalyon, division, level) {
        super(name, age, batalyon, division, level)
    }

    talk() {
        super.talk()
        if(this._division <=5) {
            console.log(`I am from division ${this._division} "${this._batalyon}" main force, ready to win`)
        } else {
            console.log(`I am from division ${this._division} "${this._batalyon}" support troops, ready to help main force`)
        }
    }
}

class Archer extends Army {
    constructor(name, age, batalyon, division, level) {
        super(name, age, batalyon, division, level)
    }

    talk() {
        super.talk()
        if(this._division <= 5) {
            console.log(`I am from division ${this._division} "${this._batalyon}" main force, ready to win`)
        } else {
            console.log(`I am from division ${this._division} "${this._batalyon}" support troops, ready to help main force`)
        }
    }
}

//export class
module.exports = {Knight, Spearman, Archer};

